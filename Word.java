
/**
 * Enumeration class Word - write a description of the enum class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public enum Word
{
    GO("go"),QUIT("quit"),HELP("help"),LOOK("look"),PICK("pick"),EAT("eat"),CHECK("check"),SEE("see");
    
    private String commandWord;
    private Word(String commandWord){
        this.commandWord = commandWord;
    }
    
    public String getCommandWord(){
        return this.commandWord;
    }
}
