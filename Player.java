import java.util.*;

public class Player
{
    public int strength = 100;
    private Map<String, Food> bag = new HashMap<>();
    private String name;
    
    
    public void pick(Food food){
        bag.put(food.getName(), food);
        name = food.getName();
    }
    
    public void eat(String name){
       Food food = bag.get(name);
       if(food != null){
           strength += food.getEnergy();
           if(strength >= 100){
               strength = 100;
               System.out.println("你再吃就撑死了！！");
           }
       }
    }
    
    public void step(){
        strength -= 10;
    }
  
    
    public String getName(){
        return name;
    }
    
    public Food getFood(){
        return bag.get(name);
    }
    
    public int getStrength(){
        return this.strength;
    }
}
