import java.util.*;
/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game 
{
    private Parser parser;
    private Room currentRoom;
    private Food bag = null;
    private Food bag1 = null;
    private int gongj = 10;
    private int fangy = 10;
    private Food xunz = null;
    private Player player;
    private boolean y = false;
    public int strength = 100;
    
    
        
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        createRooms();
        parser = new Parser();
        player = new Player();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms(){
        Room sjt,xl,sl,hb,dlb,dse,zl,xlo,cc,dlo,dsj;
      
        // create the rooms
        sjt = new Room("水晶塔",new Food("金币",50));
        xl = new Room("下路，遇到了孙膑，获得了first blood，快看看可以购买什么装备", new Food("金币",30));
        sl = new Room("上路,碰见的昔日败将钟无艳，他早已布下天罗地网，你残血！！", new Food("血量",-1000));
        hb= new Room("红BUFF野区，在与小乔法师的配合下，你将对方孙悟空干掉！。", new Food("红BUFF",50));
        dlb = new Room("对方蓝BUFF野区，对方打野不在，法师甄姬在中路，赶紧打掉蓝BUFF悄咪咪的溜了吧。",new Food("蓝BUFF",10));
        dse = new Room("对方上路二塔，见到对方小鲁班也在，直接放送集合信号，你方打野露娜闻讯赶来，你最终战胜了他们但你已是残血了，塔下有血包, ", new Food("血包",100));
        zl = new Room("中路，你方法师被两人围攻了，快去找她！", new Food("金币",30));
        xlo = new Room("小龙，遇到露娜在打小龙，你与她共同将小龙打掉，你们队伍都获得了金币！快拿着它吧！", new Food("小龙",50));
        cc = new Room("草丛，那里对方法师和辅助早已布下了种种陷阱，你的血量减少了70%。");
        dlo = new Room("大龙，你与你的队员们齐心合力，在亚瑟即将被大龙杀掉的千钧一发之际，你们将大龙干掉了！！！",new Food("大龙",80));
        dsj = new Room("对方水晶，获得大龙之后，你与队员们，勇往向前，推掉对方高地，并且你取得5杀，最终对方团灭，水晶塔破碎，你们取得这场战斗的胜利！！！");
        
        // initialise room exits
        sjt.setExit("north",zl);
        zl.setExit("east", dlo);
        dlo.setExit("north", dse);
        dlo.setExit("south", hb);
        hb.setExit("east", xl);
        dlo.setExit("east", dlb);
        dlo.setExit("south",cc);
        cc.setExit("south",sjt );
        cc.setExit("west",sl);
        zl.setExit("west",sl);
        sl.setExit("south",sjt);
        
        sjt.setExit("west", sl);
        sl.setExit("south",sjt);
        
        sjt.setExit("east", xl);
        xl.setExit("north", dse); 
        dse.setExit("north", dsj);
        
        
        currentRoom = sjt;  // start game outside
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
                
        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            
            finished = processCommand(command);
        }
        System.out.println("感谢你的参与。再见！");
        
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("欢迎来到王者荣耀!");
        System.out.println("敌军还有三秒到达战场。");
        System.out.println("如果你需要帮助就输入“help”,告诫你不要动不动就help，不然你怎么死的都不知道。");
        System.out.println();
        System.out.println("你到了" + currentRoom.getDescription());
        currentRoom.printExits();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("我不知道你什么意思。。。。");
            return false;
        }

        Word commandWord = command.getCommandWord();
        switch(commandWord){
            case HELP:
                printHelp();
                break;
            case GO:
                goRoom(command);
                player.step();
                strength-=10;
                break;
            case QUIT:
                wantToQuit = quit(command);
                break;
            case LOOK:
                Food food = currentRoom.getFood();
                if(food != null){
                    System.out.println("这里有" + food.getName());
                }
                else{
                    System.out.println("这里啥都没有。");
                }
                break;
            case PICK:
                Food food1 = currentRoom.getFood();
                if(food1.getName()=="血量回复药剂"){
                     if(bag==null){
                         bag=food1;
                         System.out.println("你捡起了"+ food1.getName());
                     }
                     else
                         System.out.println("你的超级背包满了");
                }
                else if(food1.getName()=="金币"){
                     if(bag1==null){
                         bag1=food1;
                         System.out.println("你捡起了"+ food1.getName());
                     }
                     else
                         System.out.println("你的小背包满了");
                }
                else if(food1.getName()=="蓝BUFF"){
                     gongj+=10;
                     fangy+=20;
                     System.out.println("你的攻击力增加了5%，防御力增加了2%");
                }
                else if(food1.getName()=="红BUFF"){
                     gongj+=30;
                     fangy+=5;
                     System.out.println("你的攻击力增加了10%，防御力增加了5%");
                }
                else if(food1.getName()=="大龙"){
                     if(xunz==null){
                         xunz=food1;
                         System.out.println("你获得了"+ food1.getName());
                     }
                     else
                         System.out.println("你已经有大龙了");
                }
                break;
            case EAT:
                if(bag != null){
                    System.out.println("你获得了" + bag.getName());
                    player.eat(bag.getName());
                    strength+=bag.getEnergy();
                    if(strength>100)
                    strength=100;
                    bag=null;
                }
                else if(bag1 != null){
                    System.out.println("你获得了" + bag1.getName());
                    player.eat(bag1.getName());
                    strength+=bag1.getEnergy();
                    if(strength>100)
                    strength=100;
                    bag1=null;
                }
                else{
                    System.out.println("你没有东西可以补充体力了。");
                }
                break;
            case CHECK:
                if(bag != null){
                    System.out.println("你的超级背包里有" + bag.getName());
                }
                else{
                    System.out.println("你的超级背包里屁都没有。");
                }
                if(bag1 != null){
                    System.out.println("你的小背包里有" + bag1.getName());
                }
                else{
                    System.out.println("你的小背包里屁都没有。");
                }
                break;
            case SEE:
                System.out.println("体力："+strength+"，攻击力："+gongj+"，防御力："+fangy);
        }
        if(currentRoom.getDescription()=="上路,碰见的昔日败将钟无艳，他早已布下天罗地网，你输了！！！"||currentRoom.getDescription()=="对方水晶，获得大龙之后，你与队员们，勇往向前，推掉对方高地，并且你取得5杀，最终对方团灭，水晶塔破碎，你们取得这场战斗的胜利！！！"){
            if(xunz!=null)
            System.out.println("你持有大龙通关，你将获得强大的力量！");
            wantToQuit = true;
        }
        
        if(currentRoom.getDescription()=="草丛，那里对方法师和辅助早已布下了种种陷阱，你的血量减少了70%。"){
                 gongj /= 2;
                 fangy /= 2;
        }
        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        // System.out.println("You are lost. You are alone. You wander");
        // System.out.println("around at the university.");
        // System.out.println();
        // System.out.println("Your command words are:");
        // System.out.println("   go quit help");
        System.out.println("打不过就来找我？");
        System.out.println("笑话");
        System.out.println("你以为我回帮你吗？");
        System.out.println("你有大龙吗？");
        System.out.println("算了，看你可怜");
        Word[] words = Word.values();
        for(int i=0;i<words.length;i++){
            System.out.print(words[i].getCommandWord()+" ");
            System.out.println();
        }
    }

    /** 
     * Try to go in one direction. If there is an exit, enter
     * the new room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            System.out.println("去哪儿？");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        Room nextRoom = currentRoom.goNext(direction);
        if(nextRoom == null){
            //System.out.print("There is no door!");
            System.out.println("这里出不去！");
        }
        else {
            currentRoom = nextRoom;
            System.out.println("你在" + currentRoom.getDescription());
            currentRoom.printExits();
        }
    }

    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}
