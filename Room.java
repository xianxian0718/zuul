import java.util.*;

public class Room 
{
    public String description;
    private Map<String, Room> rooms = new HashMap<>();
    private Food food = null;
    private Player player;

    
    public Room(){
        player = new Player();
    }
    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    public Room(String description) 
    {
        this.description = description;
    }
    
    public Room(String description,Food food){
        this(description);
        this.food = food;
    }

    public Room goNext(String direction){
        return rooms.get(direction);
    }
    
    public void printExits(){
        System.out.print("Exits: ");
        //player.step();
        rooms.keySet().forEach(key -> System.out.print(key + " "));
        System.out.println("");
        System.out.println("see可查看当前状态");
    }
    
    /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     */
    public void setExit(String direction, Room room){
        rooms.put(direction, room);
    }
     
    /**
    * @return The description of the room.
    */
    public String getDescription()
    {
        return description;
    }

    public Food getFood(){
        return this.food;
    }
    
    
}
